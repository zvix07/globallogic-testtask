﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Text.Json;

namespace Test_Task
{
   public class HierarchyRepresentation
    {
     

        private Folder RepresentFolder(DirectoryInfo directory)
        {
          

            string name = directory.Name;
            DateTime dateCreated =  directory.CreationTime;

            var list_of_files = directory.GetFiles();
            List<File> represented_files = null;
            if (list_of_files.Length >0)
             represented_files = new List<File>();
            foreach (var file in list_of_files)
            {
                represented_files.Add( new File { 
                                                    Name = file.Name,
                                                    Size = file.Length.ToString() + " B",
                                                    Path = file.FullName
                    });
            }

            var list_of_folders = directory.GetDirectories();
            List<Folder> represented_folders = null;
            if (list_of_folders.Length > 0)
                 represented_folders = new List<Folder>();
          
               
                foreach (var folder in list_of_folders)
                {
                    represented_folders.Add(RepresentFolder(folder));
                }
            

            return  new Folder {
                                Name = name,
                                DateCreated = dateCreated.ToString(),
                                Files = represented_files,
                                Children = represented_folders
                                };
        }

        public string BuildHirerarchy(string path)
        {
            DirectoryInfo directory;
            try
            {
               directory = new DirectoryInfo(path);
            }
            catch (Exception)
            {
                  throw new Exception("Invalid path to directory. Please select correct path!");
              //  return "Error: Invalid path to directory. Please enter correct path.";
            }
              
           
            var represented_main_folder = RepresentFolder(directory);

            return JsonSerializer.Serialize<Folder>(represented_main_folder, new JsonSerializerOptions { WriteIndented = true, IgnoreNullValues = true });
        }
    }
}
