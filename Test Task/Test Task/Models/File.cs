﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test_Task
{
   internal class File
    {
        public string Name { get; set; }
        public string Size { get; set; }
        public string Path { get; set; }
    }
}
