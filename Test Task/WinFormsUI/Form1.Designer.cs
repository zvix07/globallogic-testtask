﻿namespace WinFormsUI
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.selectPathButton = new System.Windows.Forms.Button();
            this.buildHierarchyButton = new System.Windows.Forms.Button();
            this.saveButton = new System.Windows.Forms.Button();
            this.selectedPathLabel = new System.Windows.Forms.Label();
            this.TextBox = new System.Windows.Forms.RichTextBox();
            this.saveFileDialog = new System.Windows.Forms.SaveFileDialog();
            this.SuspendLayout();
            // 
            // selectPathButton
            // 
            this.selectPathButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.selectPathButton.Location = new System.Drawing.Point(281, 409);
            this.selectPathButton.Name = "selectPathButton";
            this.selectPathButton.Size = new System.Drawing.Size(165, 29);
            this.selectPathButton.TabIndex = 0;
            this.selectPathButton.Text = "Select directory";
            this.selectPathButton.UseVisualStyleBackColor = true;
            this.selectPathButton.Click += new System.EventHandler(this.selectPath);
            // 
            // buildHierarchyButton
            // 
            this.buildHierarchyButton.Enabled = false;
            this.buildHierarchyButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buildHierarchyButton.Location = new System.Drawing.Point(452, 409);
            this.buildHierarchyButton.Name = "buildHierarchyButton";
            this.buildHierarchyButton.Size = new System.Drawing.Size(165, 29);
            this.buildHierarchyButton.TabIndex = 1;
            this.buildHierarchyButton.Text = "Represent Hierarchy";
            this.buildHierarchyButton.UseVisualStyleBackColor = true;
            this.buildHierarchyButton.Click += new System.EventHandler(this.buildHierarchy);
            // 
            // saveButton
            // 
            this.saveButton.Enabled = false;
            this.saveButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.saveButton.Location = new System.Drawing.Point(623, 409);
            this.saveButton.Name = "saveButton";
            this.saveButton.Size = new System.Drawing.Size(165, 29);
            this.saveButton.TabIndex = 2;
            this.saveButton.Text = "Save";
            this.saveButton.UseVisualStyleBackColor = true;
            this.saveButton.Click += new System.EventHandler(this.saveResult);
            // 
            // selectedPathLabel
            // 
            this.selectedPathLabel.AutoSize = true;
            this.selectedPathLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.selectedPathLabel.Location = new System.Drawing.Point(21, 19);
            this.selectedPathLabel.Name = "selectedPathLabel";
            this.selectedPathLabel.Size = new System.Drawing.Size(0, 20);
            this.selectedPathLabel.TabIndex = 3;
            // 
            // TextBox
            // 
            this.TextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.TextBox.Location = new System.Drawing.Point(9, 55);
            this.TextBox.Name = "TextBox";
            this.TextBox.ReadOnly = true;
            this.TextBox.Size = new System.Drawing.Size(764, 343);
            this.TextBox.TabIndex = 4;
            this.TextBox.Text = "";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.TextBox);
            this.Controls.Add(this.selectedPathLabel);
            this.Controls.Add(this.saveButton);
            this.Controls.Add(this.buildHierarchyButton);
            this.Controls.Add(this.selectPathButton);
            this.Name = "Form1";
            this.Text = "Hierarchy Representation";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button selectPathButton;
        private System.Windows.Forms.Button buildHierarchyButton;
        private System.Windows.Forms.Button saveButton;
        private System.Windows.Forms.Label selectedPathLabel;
        private System.Windows.Forms.RichTextBox TextBox;
        private System.Windows.Forms.SaveFileDialog saveFileDialog;
    }
}

