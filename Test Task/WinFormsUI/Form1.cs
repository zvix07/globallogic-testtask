﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Test_Task;
using System.IO;

namespace WinFormsUI
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

         string _selected_path;
        string _represented_hierarchy;
         void UpdateForm()
        {
            buildHierarchyButton.Enabled = false;
            saveButton.Enabled = false;
            TextBox.Clear();
            selectedPathLabel.Text = "";
        }
        private void selectPath(object sender, EventArgs e)
        {
            UpdateForm();
            FolderBrowserDialog folderBrowserDialog = new FolderBrowserDialog();

            if (folderBrowserDialog.ShowDialog()==DialogResult.OK)
            {
                _selected_path = folderBrowserDialog.SelectedPath;
                selectedPathLabel.Text = "Selected path: " + _selected_path;
                buildHierarchyButton.Enabled = true;
            }
        }

        private void buildHierarchy(object sender, EventArgs e)
        {
           
            var hierarchyRepresentation = new HierarchyRepresentation();
          
            try
            {
                _represented_hierarchy = hierarchyRepresentation.BuildHirerarchy(_selected_path);
                TextBox.Text = _represented_hierarchy;
                saveButton.Enabled = true;
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message, "Error");
                UpdateForm();
            }
        }

        private void saveResult(object sender, EventArgs e)
        {
            saveFileDialog.Filter = "Json files (*.json)|*.json|Text files (*.txt)|*.txt";
            if (saveFileDialog.ShowDialog() ==DialogResult.OK)
            {
                File.WriteAllText(saveFileDialog.FileName, _represented_hierarchy);
            
            }

        }
    }
}
